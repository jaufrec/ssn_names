set terminal pngcairo
set output 'avg_f.png'
set datafile separator ","
unset border 

binwidth = 100
bin(x,width)=width*floor(x/width)
plot '/tmp/average_f.csv' using (bin($1,binwidth)):(1.0) smooth freq with boxes