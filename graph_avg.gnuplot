#!/usr/bin/env gnuplot

reset

set terminal pngcairo
set output 'avg_f.png'
set datafile separator ","
unset border 

set tics font "Quadraat Sans OT,20"
set title font "Quadraat Sans OT,35"
set xlabel font "Quadraat Sans OT,25"
set ylabel font "Quadraat Sans OT,25"

unset key
set terminal png size 2000,2000

set title "Distribution of average popularity of Female Names, US" offset 0,2
set xlabel "Popular Enough" offset 0,-2
set ylabel "Rare Enough" offset 2,0

#plot [-10:140] [-10:140] '/tmp/names_f.csv' using 2:5:1 with labels rotate by 45 font "tiny,3"

w=1
plot [-10:140] [-10:140] '/tmp/names_f.csv' using ($3 + w*(rand(0)-0.5)):($2 + w*(rand(0)-0.5)):1 with labels rotate by 45 font "tiny,3"

set title "Timeless Male Names, US"
set xlabel "Too Popular"
set ylabel "Too Rare"

#plot [-10:140] [-10:140] '/tmp/names_m.csv' using 2:5:1 with labels rotate by 45 font "tiny,3"

set output 'test1.png'

w=1
plot [-10:140] [-10:140] '/tmp/names_m.csv' using ($2 + w*(rand(0)-0.5)):($5 + w*(rand(0)-0.5)):1 with labels rotate by 45 font "tiny,3"
