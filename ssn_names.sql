create table raw_names (
       year int,
       gender varchar(1),
       name varchar(15),
       count int,
       rank int
);

create table names (
       name varchar(15),
       gender varchar(1),
       num_above_max int default 0,
       num_below_min int,
       total_count int,
       total_count_below int default 0
)


/*
Commands work in progress

# ranked list by year and gender
SELECT name, 
       gender,
       rank() over (order by count desc) as rank
FROM names where year = 1880 and gender = 'F';

# update in place with rank
UPDATE names
       set rank = r.rnk
FROM (
     SELECT name, year, gender, rank() OVER (partition by year, gender ORDER BY count DESC) AS rnk 
     FROM names
     ) r
WHERE names.name = r.name and names.gender = r.gender and names.year = r.year




# ranked name list
SELECT name,rank FROM (
  SELECT name, rank() OVER (ORDER BY count desc) FROM names
)

##########
# count of above max and below min

# working tables:
select name, rank into name_max_50_m from names where rank <= 50 and gender = 'M';
select name, rank into name_min_500_m from names where rank > 500 and gender = 'M';

SELECT name,
       (SELECT count(name)
          FROM name_max_50_f nm5f
         WHERE nm5f.name=n.name) as over_50,
       (SELECT count(name)
          FROM name_min_500_f nm50f
         WHERE nm50f.name=n.name) as under_500,
       (SELECT count(name)
          FROM names ni
         WHERE gender = 'F' and ni.name=n.name ) as total_count
  INTO name_with_counts_f
  FROM names n
 WHERE gender = 'F'
 GROUP BY name 

 update name_with_counts set combined_under = (133 - total_count) + under_500;

SELECT name,
       (SELECT count(name)
          FROM name_max_50_m nm5m
         WHERE nm5m.name=n.name) as over_50,
       (SELECT count(name)
          FROM name_min_500_m nm50m
         WHERE nm50m.name=n.name) as under_500,
       (SELECT count(name)
          FROM names ni
         WHERE gender = 'M' and ni.name=n.name ) as total_count
  INTO name_with_counts_m
  FROM names n
 WHERE gender = 'M'
 GROUP BY name 

# dump to CSV for graphing
copy (select * from name_with_counts_m where combined_under<133) to '/tmp/names_m.csv' with CSV;


###################
# build a list:

# first, get a list of names

select distinct name, gender into name_list from names;

# second, make a table with the cartesian product of names and years, so that it includes everything:

SELECT nl.name,
       nl.gender,
       y
  FROM name_list nl
 CROSS JOIN
  generate_series(1880,2012) y 

*/

