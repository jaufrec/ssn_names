#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import csv
import glob
import string
import numpy

missing_rank = 15000

try:
    
    con = psycopg2.connect(database='ssn_names')
    cur = con.cursor()
    list_of_names_statement = """
SELECT name, total_count as count
  FROM names_with_counts_m
"""
    
    cur.execute(list_of_names_statement)
    names=cur.fetchall()

    for row in names:
        name = row[0]
        count = row[1]

        # get the median
        name_median_query = """
SELECT median(rank)
  FROM names 
 WHERE name='%s' and gender = 'M'
""" % name
        cur.execute(name_median_query)
        median = cur.fetchall()[0][0]

        # calculate abs for each year and sum them

        name_rank_query = """
SELECT rank
  FROM names
 WHERE name='%s'
   AND gender = 'M'
""" % name

        cur.execute(name_rank_query)
        ranks = cur.fetchall()

        abs_sums = []
        
        for rank in ranks:
            deviation = int(abs( rank[0] - median ))

            abs_sums.append(deviation)
        
        abs_sums=sorted(abs_sums)

        if len(abs_sums) % 2 == 0:
            x = len(abs_sums) / 2
            a, b = abs_sums[x-1], abs_sums[x]
            mad = (a + b) / 2
        else:
            middle = len(abs_sums) // 2
            mad = abs_sums[middle]
        
        print "%s, %s, %s, %s" % (name, count, median, mad)
        

except psycopg2.DatabaseError, e:
    print 'Error %s' % e    
    sys.exit(1)
        
