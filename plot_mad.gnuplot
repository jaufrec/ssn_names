#!/usr/bin/env gnuplot

reset

set terminal pngcairo
set output 'test_mad.png'
set datafile separator ","
unset border 

set tics font "Quadraat Sans OT,20"
set title font "Quadraat Sans OT,35"
set xlabel font "Quadraat Sans OT,25"
set ylabel font "Quadraat Sans OT,25"

unset key
set terminal png size 2000,2000

set title "Timelessness of Female Names, US, 1880 to 2012" offset 0,2
set xlabel "Years of Popularity" offset 0,-2
set ylabel "Consistency" offset 2,0

set xrange [] reverse
set yrange [] reverse
set logscale y 

set palette rgb 7, 5, 15
show palette gradient

w=0
plot [1:150] [0.1:30000] '~/ssn_names/names_mad_all_f.csv' using ($2 + w*(rand(0)-0.5)):($3 + w*(rand(0)-0.5)):1 with labels rotate by 45 font "tiny,3" 

#plot [150:1] [0.1:20000] '~/ssn_names/names_mad_all_f.csv' using ($2 + w*(rand(0)-0.5)):($3 + w*(rand(0)-0.5)):4 with points pt 7 ps 2  palette

