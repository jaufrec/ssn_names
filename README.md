Tools for manipulating Social Security name data.
Data from http://www.ssa.gov/oact/babynames/limits.html

# Getting Started
## Prerequisites

1. `sudo apt install postgresql python-psycopg2`
2. `sudo su - postgres`
  1. `createuser -s <yourname>`
  2. `exit`
3. `createdb ssn_names`

## Importing data
Load the raw data provided by SSN into a PostGreSQL database

### Prepare database
1. For Kubuntu 13.10:
2. `sudo apt install wget`
3. Prepare the database
  1. Assuming that the current user has superuser access to postgresql:
  2. `psql -d ssn_names -f ssn_names.sql`

### Get the raw data and load it into postgresql.  Note that one of the SQL commands in import_names may take 10+ minutes to execute.
Execute these commands in a bash shell:

```TEMPDIR=`mktemp -d`
OLD_DIR=`pwd`
pushd $TEMPDIR
wget http://www.ssa.gov/oact/babynames/names.zip
unzip names.zip
python ${OLD_DIR}/import_names.py
popd
rm -rf ${TEMPDIR}```

# Analyze the data

